export interface BridgeMetadata {
    control_url: string;
};

export interface BridgeIndexData {
    owner: string;
    name: string;
    id: string;
    service_id: string;
    is_connetcted: boolean;
}