import { environment } from '../environments/environment';

const ApiHost = environment.ApiHost;
const ApiRoot = ApiHost + '/api/v0';

export { ApiHost, ApiRoot };
