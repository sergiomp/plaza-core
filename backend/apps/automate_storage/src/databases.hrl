%% Installation meta-data
-define(INSTALLATION_CONFIGURATION_TABLE, automate_installation_configuration).

%% Structures
-define(REGISTERED_USERS_TABLE, automate_registered_users).
-define(USER_SESSIONS_TABLE, automate_user_sessions).
-define(USER_MONITORS_TABLE, automate_user_monitors).
-define(USER_PROGRAMS_TABLE, automate_user_programs).
-define(RUNNING_PROGRAMS_TABLE, automate_running_programs).
-define(PROGRAM_TAGS_TABLE, automate_program_tags).
-define(RUNNING_THREADS_TABLE, automate_running_program_threads).

-define(PROGRAM_VARIABLE_TABLE, automate_program_variable_table).
